'''
Template Component main class.

'''

import logging
import os
import sys
from pathlib import Path
import csv
import json
from datetime import datetime

from kbc.env_handler import KBCEnvHandler

# configuration variables
KEY_API_TOKEN = '#api_token'
KEY_PRINT_HELLO = 'print_hello'

# #### Keep for debug
KEY_DEBUG = 'debug'

MANDATORY_PARS = [KEY_API_TOKEN, KEY_API_TOKEN]
MANDATORY_IMAGE_PARS = []

APP_VERSION = '0.0.1'


class Component(KBCEnvHandler):

    def __init__(self, debug=False):
        # for easier local project setup
        default_data_dir = Path(__file__).resolve().parent.parent.joinpath('data').as_posix() \
            if not os.environ.get('KBC_DATADIR') else None

        KBCEnvHandler.__init__(self, MANDATORY_PARS, log_level=logging.DEBUG if debug else logging.INFO,
                               data_path=default_data_dir)
        # override debug from config
        if self.cfg_params.get(KEY_DEBUG):
            debug = True
        if debug:
            logging.getLogger().setLevel(logging.DEBUG)
        logging.info('Running version %s', APP_VERSION)
        logging.info('Loading configuration...')

        
        # ####### EXAMPLE TO REMOVE
        # intialize instance parameteres

        # ####### EXAMPLE TO REMOVE END

    def run(self):
        '''
        Main execution code
        '''
        params = self.cfg_params  # noqa

        # get filepath of first input file
        SOURCE_FILE_PATH = self.get_input_tables_definitions()[0].full_path
        # set result file path
        RESULT_FILE_PATH = self.tables_out_path + "/output.csv"
        # set table manifest with incremenatl load an row_number as PK
        self.configuration.write_table_manifest(file_name=RESULT_FILE_PATH,
                                                primary_key=["row_number"],
                                                incremental=True)
        last_state = self.get_state_file()
        print("last update: ", last_state)
        now_str = str(datetime.now().date())
        self.write_state_file({"last_update": now_str})
        print("result: ", RESULT_FILE_PATH)
        print("source file: ", SOURCE_FILE_PATH)
        DATA_FOLDER = Path('/data')
        config = json.load(open(DATA_FOLDER.joinpath('config.json')))
        PARAM_PRINT_LINES = config['parameters']['print_rows']
        with open(SOURCE_FILE_PATH, 'r') as input, open(RESULT_FILE_PATH, 'w+', newline='') as out:
            reader = csv.DictReader(input)
            new_columns = reader.fieldnames
            # append row number col
            new_columns.append('row_number')
            writer = csv.DictWriter(out, fieldnames=new_columns, lineterminator='\n', delimiter=',')
            writer.writeheader()
            for index, l in enumerate(reader):
                # print line
                if PARAM_PRINT_LINES:
                    print(f'Printing line {index}: {l}')
                # add row number
                l['row_number'] = index
                writer.writerow(l)


"""
        Main entrypoint
"""
if __name__ == "__main__":
    if len(sys.argv) > 1:
        debug_arg = sys.argv[1]
    else:
        debug_arg = False
    try:
        comp = Component(debug_arg)
        comp.run()
    except Exception as exc:
        logging.exception(exc)
        exit(1)
